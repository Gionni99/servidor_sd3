/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

/**
 * Classe para preenchimento dos dados das vagas
 * @author Giovanni e Steven
 */
public class VagaEmprego {//Classe para  salvar  as  vagas ofertadas
    //dados
    String nomeDaEmpresa;
    String contato;
    String areaDaVaga;
    Integer cargaHoraria;
    Integer salario;
    
    
    /**
     * Construtor
     * @param nomeDaEmpresa
     * @param contato
     * @param areaDaVaga
     * @param cargaHoraria
     * @param salario
     */
    
    public VagaEmprego(String nomeDaEmpresa, String contato, String areaDaVaga, Integer cargaHoraria, Integer salario){
        this.contato = contato;
        this.areaDaVaga = areaDaVaga;
        this.cargaHoraria = cargaHoraria;
        this.salario = salario;
        
    }
    
}
