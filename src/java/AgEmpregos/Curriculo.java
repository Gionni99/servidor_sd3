/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

/**
 * Classe para preenchimento do curriculo
 * @author Giovanni e Steven
 */
public class Curriculo {
    //dados do curriculo
    String nome;
    String contato;
    String areaDeInteresse;
    Integer cargaHoraria;
    Integer salarioPretendido;
    
    
    /**
     * Construtor
     * @param nome
     * @param contato
     * @param areaDeInteresse
     * @param cargaHoraria
     * @param salarioPretendido
     */
    
    public Curriculo(String nome,String contato,String areaDeInteresse, Integer cargaHoraria, Integer salarioPretendido) {
        this.nome= nome;
        this.contato = contato;
        this.areaDeInteresse = areaDeInteresse;
        this.cargaHoraria = cargaHoraria;
        this.salarioPretendido = salarioPretendido;
    }
    
}
