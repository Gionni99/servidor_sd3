/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.util.ArrayList;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 * Classe para oferecer o servico web aos clientes
 * @author Giovanni e Steven
 */
@WebService(serviceName = "AgenciaEmpregosWebService")
public class AgenciaEmpregosWebService1 {
    private List<Curriculo> curriculos = new ArrayList<Curriculo>();
    private List<VagaEmprego> vagas = new ArrayList<VagaEmprego>();

    /**
     * Consulta por area de interesse
     * Web service operation
     * @param areaDeInteresse
     * @return String unica com todas as vagas 
     */
    @WebMethod(operationName = "ConsultaVaga")
    public String ConsultaVaga(@WebParam(name = "areaDeInteresse") String areaDeInteresse) {
        try{
            // String de retorno de informacoes, completo
            String Resposta = new String();
            VagaEmprego vagaAtual;
            int encontrado = 0;
            // Busca por comparacao
            for(int i = 0;i<vagas.size();i++){
                vagaAtual = vagas.get(i);
                // Encontrou a vaga
                if(vagaAtual.areaDaVaga.startsWith(areaDeInteresse)){
                    Resposta = Resposta + "\n [["+
                            vagaAtual.nomeDaEmpresa+"/"+
                            vagaAtual.contato+"/"+
                            vagaAtual.areaDaVaga+"/"+
                            vagaAtual.cargaHoraria.toString()+"/"+
                            vagaAtual.salario.toString()+"]]    ";
                    encontrado = 1;
                }
            }
            if(encontrado ==1)
                return Resposta;
            else
                return "Não encontrado";
        }catch(Exception e){
            return e.getMessage();
        }
    }

    /**
     * Classe para cadastrar curriculos
     * Web service operation
     * @param Nome
     * @param Contato
     * @param AreaDeInteresse
     * @param CargaHoraria
     * @param SalarioPretendido
     * @return Mensagem de confirmacao
     */
    @WebMethod(operationName = "CadastroCurriculo")
    public String CadastroCurriculo(@WebParam(name = "Nome") String Nome, @WebParam(name = "Contato") String Contato, @WebParam(name = "AreaDeInteresse") String AreaDeInteresse, @WebParam(name = "CargaHoraria") Integer CargaHoraria, @WebParam(name = "SalarioPretendido") Integer SalarioPretendido) {
        try{
            // String para retornar
            String Resposta = new String();
            // Novo curriculo
            Curriculo curriculo = new Curriculo(Nome,Contato,AreaDeInteresse,CargaHoraria,SalarioPretendido);
            Curriculo curriculoAtual;
            int encontrada = 0;
            // Busca para alterar curriculos existentes
            for(int i=0;i<curriculos.size();i++){
                curriculoAtual=curriculos.get(i);
                if(curriculoAtual.nome.equals(Nome)) {
                    curriculos.set(i, curriculo);
                    encontrada = 1;
                    Resposta = "Cadastro Alterado Com Sucesso!";
                }
            }
            if(encontrada==0){
                curriculos.add(curriculo);
                Resposta = "Cadastro Concluído Com Sucesso!";
            }
            
            return Resposta;
        } catch(Exception e) {
            return e.getMessage();
        }
    }
    
    /**
     * Classe para consultar os curriculos
     * Web service operation
     * @param AreaDeInteresse
     * @return String unica com todos os curriculos
     */
    @WebMethod(operationName = "ConsultaCurriculo")
    public String ConsultaCurriculo(@WebParam(name = "AreaDeInteresse") String AreaDeInteresse) {
        try{
            //String para retornar
            String Resposta = new String();
            Curriculo curriculoAtual;
            int encontrado = 0;
            // Loop de busca
            for(int i = 0;i<curriculos.size();i++){
                curriculoAtual = curriculos.get(i);
                //Anota se encontrar
                if(curriculoAtual.areaDeInteresse.startsWith(AreaDeInteresse)){
                    Resposta = Resposta + "\n [["+
                            curriculoAtual.nome+"/"+
                            curriculoAtual.contato+"/"+
                            curriculoAtual.areaDeInteresse+"/"+
                            curriculoAtual.cargaHoraria.toString()+"/"+
                            curriculoAtual.salarioPretendido.toString()+"]]";
                    encontrado = 1;
                }
            }
            if(encontrado ==1)
                return Resposta;
            else
                return "Não encontrado";
        }catch(Exception e){
            return e.getMessage();
        }
    }

    /**
     * Classe para fazer o cadastro das vagas
     * Web service operation
     * @param NomeEmpresa
     * @param Contato
     * @param AreaDaVaga
     * @param CargaHoraria
     * @param Salario
     * @return Mensagem de confirmacao
     */
     
    @WebMethod(operationName = "CadastroVaga")
    public String CadastroVaga(@WebParam(name = "NomeEmpresa") String NomeEmpresa, @WebParam(name = "Contato") String Contato, @WebParam(name = "AreaDaVaga") String AreaDaVaga, @WebParam(name = "CargaHoraria") Integer CargaHoraria, @WebParam(name = "Salario") Integer Salario) {
        try{
            //String de resposta
            String Resposta = new String();
            // Nova caga
            VagaEmprego vaga = new VagaEmprego(NomeEmpresa,Contato,AreaDaVaga,CargaHoraria,Salario);
            VagaEmprego vagaAtual;
            int encontrada = 0;
            // Busca por vagas existentes para alterar
            for(int i=0;i<vagas.size();i++){
                vagaAtual=vagas.get(i);
                // Se encontrou, altera
                if(vagaAtual.nomeDaEmpresa.equals(NomeEmpresa)) {
                    vagas.set(i, vaga);
                    encontrada = 1;
                    Resposta = "Cadastro Alterado Com Sucesso!";
                }
            }
            //Senao adiciona novo
            if(encontrada==0){
                vagas.add(vaga);
                Resposta = "Cadastro Concluído Com Sucesso!";
            }
            
            return Resposta;
        }catch(Exception e){
            return e.getMessage();
        }
    }
    

}
